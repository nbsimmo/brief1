<?php
get_header();
?>
             <section class="home">
                <nav>
                    <a href="">Home</a>
                    <a href="">About Me</a>
                    <a href="">Skills</a>
                    <a href="">Contact Me</a>
                </nav>
                <div id="quote">
                    <p>"Web Developer" [We.b Dev.l.o.per]</p>
                    <p>1. An organism which turns Tea into websites.</p>
                </div>
                <div class="name">
                    <h1>Nathan Simmons</h1>
                    <p>Front-end Developer and Flat-UI Lover.</p>
                </div>
            </section>
            <section class="images">
                <div class="img">
                    <img src="images/pic1.jpg" alt="My Items">
                </div>
                <div class="img">
                    <img src="images/pic2.jpg" alt="My Items">
                </div>
            </section>
            <section class="about">
                <div class="img-about">
                </div>
                <div class="text">
                    <h1>About Me.</h1>
                    <p>I'm Nathan, and this is my story.</p>
                    <p>If we rewind back, it all started when I was young and gained a keen interest in computers.
                    In which I was majorly inspired by my Grandfather.</p>
                    <p>Funny enough networking was my pathway of choice, but this spiralled into a passion for web.
                    As a concept, this was new to me and everyone around me, so it was a chance to finally look cool and say "Hey, look what I can do!".</p>
                    <p>I have studied at Edge Hill University and had experience working for ElectricCircus - a design agency based in Manchester. This has enabled me to gather many skills which have aided in my time throughout web development.</p>
                    <p>I plan to further develop myself, not only as an individual, but also in my skills and experience within the industry.</p>
                    <p>Below are just some of the skills I have aquired within my time of web development.</p>
                    <div class="skills">
                        <span class="skill">JavaScript</span>
                        <span class="skill">PHP</span>
                        <span class="skill">Laravel</span>
                        <span class="skill">Ember.js</span>
                        <span class="skill">HTML5</span>
                        <span class="skill">CSS3</span>
                        <span class="skill">Photoshop</span>
                        <span class="skill">Fireworks</span>
                        <span class="skill">Illustrator</span>
                    </div>
                </div>

            </section>
            <section class="work">
            <h1>Work</h1>
            <br>
               <a class="worklink" href="#"> <div class="cont">
                    <div class="workimg">
                        <img src="images/work1.PNG" alt="">
                    </div>
                    <div class="worktext">
                        <h1>"Modulus" - A Combination of Modulisation and Automation</h1>
                        <p>Modulus is a project based on trying to simplify the process tutors take in order to create handbooks for student's modules.</p>
                        <p>Fully dynamic, the system allows for storing, reading, creating and deleting all whilst suppling a secure login.</p>
                        <p>It uses the Laravel framework with generic HTML5 and CSS3 to create a system which is not only secure but also easy to create.</p>
                        <p>The user interface is inspired from both a modern perspective but also an academia perspective - trying to fuse the two together without being too modern or too basic.</p>
                        <p>It's the ultimate fusion of modern web and academia.</p>
                    </div>
                </div></a>
                <div class="cont">
                    <div class="worktext">
                        
                    </div>
                    <div class="workimg">
                        
                    </div>
                </div>
                <div class="cont">
                    <div class="workimg">
                        
                    </div>
                    <div class="worktext">
                        
                    </div>
                </div>
            </section>
            <section class="contact">
                <div class="form-wrap">
                    <form action="">
                        <label for="fname">Name</label>
                        <input type="text" name="fname" id="fname">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" id="email">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" id="phone">
                        <label for="comment">Comment</label>
                        <textarea name="comment" id="" cols="30" rows="10"></textarea>
                        <input type="submit" value="Submit">
                        <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </form>
                </div>
                <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </section>
        </section>
<?php
get_footer();